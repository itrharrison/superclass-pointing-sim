import numpy as np
from emerlin_antenna import *

#from astropy import units as uns
#from astropy.coordinates import SkyCoord

n_epochs = 1
t_int = 1.
n_points = 7
t_on_cal = 60.*2.5
t_on_source = 60.*12.5
t_offset = t_on_source+t_on_cal

'''
n_ifs = 8
n_chan = 512
bw = 512.e6
base_freq = 1.254463e9
channel_width = 125.e3
channel_separation = 125.e3
if_width = bw / n_ifs
'''

n_ifs = 8
n_chan = 512
bw = 512.e6
base_freq = 1.254463e9
channel_width = 125.e3
channel_separation = 125.e3
if_width = bw / n_ifs

msname = '/local/scratch/harrison/test_pointings.ms'
sm.open(msname)

pointings_list = np.array(['M27',
                           'M29',
                           'L28',
                           'L26',
                           'M25',
                           'N26',
                           'N28'])

pointings_to_simulate = np.array(['M27'])

coords_list = {'M27' : me.direction('J2000', '156.103333deg', '68.110000deg'),
               'M29' : me.direction('J2000', '156.103333deg', '68.205000deg'),
               'L28' : me.direction('J2000', '156.324462deg', '68.157500deg'),
               'L26' : me.direction('J2000', '156.323551deg', '68.062500deg'),
               'M25' : me.direction('J2000', '156.103333deg', '68.015000deg'),
               'N26' : me.direction('J2000', '155.883115deg', '68.062500deg'),
               'N28' : me.direction('J2000', '155.882204deg', '68.157500deg')}

t_today = qa.convert(qa.quantity('today'), 's')['value']
epoch_start_times = np.arange(0, 4.*15.*60.*60., t_offset)

for i in np.arange(1,n_ifs+1):
  fr = base_freq + if_width*(i-1)
  sm.setspwindow(spwname = 'IF'+str(i),
                 freq = str(fr)+'Hz', # starting frequency
                 deltafreq = str(channel_separation)+'Hz', # increment per chan
                 freqresolution = str(channel_width)+'Hz', # width per chan
                 nchannels = n_chan,
                 stokes = 'I')

observatory = 'e-MERLIN'
posemerlin = me.observatory(observatory)
sm.setconfig(telescopename = 'e-MERLIN',
             x = emerlin_xx,
             y = emerlin_yy,
             z = emerlin_zz,
             dishdiameter = emerlin_diam.tolist(),
             mount = 'alt-az',
             coordsystem = 'global')
sm.setfeed(mode='perfect R L')

for i_epoch in np.arange(n_epochs):

  pointings_list_epoch = np.roll(pointings_list, 2*i_epoch+1)

  for i_point, pointing in enumerate(pointings_list_epoch):
    
    pointing_start_times = epoch_start_times + t_offset*i_point
    
    #source_ra = coords_list[pointing].ra.hms.h+'h'+coords_list[pointing].ra.hms.m+'m'+coords_list[pointing].ra.hms.s+'s'
    #source_dec = '+'+coords_list[pointing].dec.dms.d+'d'+coords_list[pointing].dec.dms.m+'m'+coords_list[pointing].dec.dms.s+'s'
    #source_dirn = me.direction('J2000', source_ra, source_dec)
    source_dirn = coords_list[pointing]
    
    if pointing in pointings_to_simulate:
      print('making uv coverage!')


      sm.setfield(sourcename = pointing,
                  sourcedirection = source_dirn)

      this_pointing_start_times = pointing_start_times[i_point::n_points]

      for t_start in this_pointing_start_times:
        
        t_start = qa.quantity(str(t_start+t_today)+'s')
        t_start = qa.convert(t_start, 'd')

        sm.settimes(integrationtime = str(t_int)+'s',
                    usehourangle = False,
                    referencetime = me.epoch('UTC', t_start))

        for i in np.arange(1,n_ifs+1):
          sm.observe(pointing, 'IF'+str(i),
                     starttime = '0s',
                     stoptime = str(t_on_source)+'s')
          print(i_epoch, pointing, qa.convert(t_start, 's')['value'], i)

sm.done()
